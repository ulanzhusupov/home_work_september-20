void main(List<String> arguments) {
  // print(generalNumbers());
  // print(getEvenNumbers());
  print(getCharacterCount("c", "Chamber of secrets"));
}

List<int> generalNumbers() {
  List<int> a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
  List<int> b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

  List<int> output = [];

  for (int i = 0; i < a.length; i++) {
    if (b.contains(a[i])) {
      output.add(a[i]);
    }
  }

  return output.toSet().toList();
}

List<int> getEvenNumbers() {
  List<int> a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100];
  List<int> output = [];

  for (int i = 0; i < a.length; i++) {
    if (a[i] % 2 == 0) output.add(a[i]);
  }

  return output;
}

int getCharacterCount(String a, String b) {
  int count = 0;

  for (int i = 0; i < b.length; i++) {
    if (b[i] == a) count++;
  }

  return count;
}
